//
//  ViewController.m
//  DMCategory
//
//  Created by chris on 15/7/21.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *urlStr = @"https://www.google.co.jp/searchq=w3c+school&oq=w3c+school&aqs=chrome..69i57.14416j0j8&sourceid=chrome&es_sm=91&ie=UTF-8";
    NSURL *url = [NSURL URLWithString:urlStr];
    NSLog(@"url = %@",url);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
