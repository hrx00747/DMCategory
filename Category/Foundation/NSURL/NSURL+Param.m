//
//  NSURL+Param.m
//  iOS-Categories (https://github.com/shaojiankui/iOS-Categories)
//
//  Created by Jakey on 14/12/30.
//  Copyright (c) 2014年 www.skyfox.org. All rights reserved.
//

#import "NSURL+Param.h"

@implementation NSURL (Param)
- (NSDictionary *)parameters
{
    NSMutableDictionary * parametersDictionary = [NSMutableDictionary dictionary];
    NSArray * queryComponents = [self.query componentsSeparatedByString:@"&"];
    for (NSString * queryComponent in queryComponents) {
        NSArray *array = [queryComponent componentsSeparatedByString:@"="];
        if(array.count==2){
            NSString *key = array[0];
            NSString *value = array[1];
            parametersDictionary[key] = value;
        }
    }
    return parametersDictionary;
}

- (NSString *)parameterForKey:(NSString *)key
{
    return [[self parameters] objectForKey:key];
}
@end
