//
//  UIColor+Extension.h
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

+ (UIColor*) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b;

+ (UIColor*) colorWithString:(NSString *)string;   //format "0xFFFFF"

@end