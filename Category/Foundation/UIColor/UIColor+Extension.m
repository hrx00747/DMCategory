//
//  UIColor+Extension.m
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "UIColor+Extension.h"

@implementation UIColor (Extension)

+ (UIColor*) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b{
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0];
}

+ (UIColor*) colorWithString:(NSString *)string{
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    unsigned hex;
    BOOL success = [scanner scanHexInt:&hex];
    
    if(!success)    return nil;
    
    CGFloat red   = ((hex & 0xFF0000) >> 16) / 255.0f;
    CGFloat green = ((hex & 0x00FF00) >>  8) / 255.0f;
    CGFloat blue  =  (hex & 0x0000FF) / 255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}


@end
