//
//  UIView+Frame.h
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property(nonatomic, assign)    CGPoint     origin;
@property(nonatomic, assign)    CGFloat     originX;
@property(nonatomic, assign)    CGFloat     originY;

@property(nonatomic, assign)    CGSize      size;
@property(nonatomic, assign)    CGFloat     sizeW;
@property(nonatomic, assign)    CGFloat     sizeH;

//@property(nonatomic, assign)    CGPoint     center;
@property(nonatomic, assign)    CGFloat     centerX;
@property(nonatomic, assign)    CGFloat     centerY;

@end
