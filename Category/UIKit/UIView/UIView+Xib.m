//
//  UIView+Xib.m
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "UIView+Xib.h"

@implementation UIView (Xib)

+ (UIView*) viewFromXib{
    
    UIView *result = nil;
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass([self class]) owner: nil options: nil];
    for (id anObject in elements) {
        if ([anObject isKindOfClass:[self class]]) {
            result = anObject;
            break;
        }
    }
    return result;
}


@end
