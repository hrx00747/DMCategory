//
//  UIView+Frame.m
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

#pragma mark - origin
- (CGPoint) origin{
    
    return self.frame.origin;
}

- (void) setOrigin:(CGPoint)origin{
    
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
    
}

- (CGFloat) originX{
    
    return self.frame.origin.x;
}

- (void) setOriginX:(CGFloat)originX{
    
    CGRect frame = self.frame;
    frame.origin.x = originX;
    self.frame = frame;
}

- (CGFloat) originY{
    
    return self.frame.origin.y;
}

- (void) setOriginY:(CGFloat)originY{
    
    CGRect frame = self.frame;
    frame.origin.y = originY;
    self.frame = frame;
}

#pragma mark - size
- (CGSize) size{
    
    return self.frame.size;
}

- (void) setSize:(CGSize)size{
    
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGFloat) sizeW{
    
    return self.frame.size.width;
}

- (void) setSizeW:(CGFloat)sizeW{
    
    CGRect frame = self.frame;
    frame.size.width = sizeW;
    self.frame = frame;
}

- (CGFloat) sizeH{
    
    return self.frame.size.height;
}

- (void) setSizeH:(CGFloat)sizeH{
    
    CGRect frame = self.frame;
    frame.size.height = sizeH;
    self.frame = frame;
}

#pragma mark - center
- (CGFloat) centerX{
    
    return self.center.x;
}

- (void) setCenterX:(CGFloat)centerX{
    
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat) centerY{
    
    return self.center.y;
}

- (void) setCenterY:(CGFloat)centerY{
    
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}
@end
