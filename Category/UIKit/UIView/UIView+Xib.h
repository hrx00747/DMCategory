//
//  UIView+Xib.h
//  DMCategory
//
//  Created by chris on 15/7/24.
//  Copyright (c) 2015年 dmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Xib)

+ (UIView*) viewFromXib;

@end
